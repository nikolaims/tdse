function [d0R, d1R, d2R] = getRSet(R, gamma, constantNegative)
%GETRSET ���������� ����������� 0, 1 � 2 ������� ������� R
%   �� ���� �������� ����������������� ������� � ������������� ��������
%   ��������� gamma
%   ���� constantNegative = true �� R(t) = R(0), R'(t)=R''(t)=0 ��� t<0
if nargin < 3
    constantNegative = false;
end
syms tau;
d0R = @(t) R(t,gamma)+t.*0;
d1R = matlabFunction(diff(d0R(tau)));
if strncmp('@()',func2str(d1R),3)
    d1R = @(t) t.*0.+d1R();
end    
d2R = matlabFunction(diff(d1R(tau)));
if strncmp('@()',func2str(d2R),3)
    d2R = @(t) t.*0.+d2R();
end
if constantNegative
   d0R = @(t) d0R(t).*heaviside(t)+d0R(0).*(1-heaviside(t));
   d1R = @(t) d1R(t).*heaviside(t);
   d2R = @(t) d2R(t).*heaviside(t);
end
end

