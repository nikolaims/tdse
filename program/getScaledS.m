function S = getScaledS(n, h, R, d2Rdt2, tmin, tmax, x)
%GETH ��������� ���������� �������� �������� ������� n
%   ���������� �������� S ����������� �� ���������� �� tmin �� tmax
%   ��������� tmax = tmin + h. R, d2Rdt2 ���������� ��� ����������
%   ���������� �� �������.
H = getH(n, h);
S = H.*integral(@(t) R(t).^-2, tmin, tmax);
S = S+0.5*integral(@(t) R(t).*d2Rdt2(t), tmin, tmax)*diag(x.^2);
S = -1i.*S;
end

