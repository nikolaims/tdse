function psiNext = oneTimeStep(psi, S)
%ONETIMESTEP ��������� ��� �� ������� psi(t+dt)=exp(S)*psi(t)
%   ����������� �������� ������� �������������� ���������� ���� ��
%   �������. ������� ��������� - �������� ������� psi � ������ 
%   ������� t � �������� �������� S.
n = length(psi);
A = -0.5.*S+eye(n);
b = (0.5.*S+eye(n))*psi;
% ������� ������� ���� A*psiNext=b
psiNext = A\b;
end
