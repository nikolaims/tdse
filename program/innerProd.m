function value = innerProd(f,g,xmax)
%INNERPROD ��������� ������������ <f|g>
%   xmax ������ �������� �������������� [-xmax, xmax]
%   ���� ����� ���� �������� �� ����������� ����� <f|f>
if nargin < 3
    xmax = 100;
end
if nargin == 1
    g = f;
end
value = integral(@(x) f(x).*conj(g(x)),-xmax,xmax);
end

