function [phi, xi] = transformScale(psi, x, R, dRdt, inverse)
%TRANSFORMSCALE ��������� ������ � �������� ���������� �������������
%   ������ �������������� ������������ ���������� invers, ����������� 
%   ��������� ��������:
%       false (��� �� �����) - ������ ��������������
%       true - ��������
if nargin < 5
    inverse = false;
end
if ~inverse
    xi = x./R;
    phi = exp(-0.5i*R*dRdt.*xi.^2).*R.^0.5.*psi;
else
    xi = x.*R;
    phi = exp(0.5i*R*dRdt.*x.^2)./R.^0.5.*psi;
end

end

