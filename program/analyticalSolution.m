function psi = analyticalSolution(x, t, v, w)
%ANALYTICALSOLUTION ������ ������������� �������
%   �������� ����� ��� t, ������������ �����������:
%       * v - ������� �������� ������
%       * w - ������� ������ (���������)
if nargin < 3
    v = 1;
    w = 1;
end
psi = exp(1i.*v.*x-(1i/2)*v^2*t-1/2*(w-1i*t)/(w^2+t^2).*(x-v*t).^2);
N = (w/pi/(w^2+t^2))^0.25;
psi = N*psi;
end