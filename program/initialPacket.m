function psi0 = initialPacket(x, v, w)
%INITIALPACKET ������ ��������� �������� �����
%   �������� ����� ��� t=0, ������������ �����������:
%       * v - ������� �������� ������
%       * w - ������� ������ (���������)
if nargin < 2
    v = 1;
    w = 1;
end
psi0 = (w*pi)^-0.25.*exp(1i.*v.*x-x.^2./(2*w));
end

