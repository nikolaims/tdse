%% ���������� ��������������� ��������� ���������� � ��������������� ��������
% � ������ ������� �������� �������������� ��������� ���������� � ������� 
% ����������� � ������������ ���������� �������� ��������� ������
% ������� ������-��������� � ����������������� ��������������. ��� ����
% ������������ ��������� �������������� ���� 
% 
% $$x = R(t)\xi$$ 
% 

%% ����������� �������� ���������� ������
% � ������ ������� ���������������� �������� ��������� ������ � �����
%%%
% * ��������� ������������ �����:
xi.h = 0.1; % ���
xi.min = -10; % ����� �������
xi.max = 30; % ������ �������
xi.net = (xi.min:xi.h:xi.max)'; % �����
xi.n = length(xi.net); % ���������� �����
%%%
% * ��������� ��������� �����: 
t.h = 0.1; % ���
t.n = 200; % ���������� �����
t.net = t.h.*(0:t.n)'; % ����� �� �������
t.max = t.net(t.n);  % ������������ �����
%%%
% * ��������� ����������� ��������������:
R.gamma = 30;
% R.model = @(t,gamma) 1+t.*gamma;
R.model = @(t,gamma) (1+t.^4.*gamma).^0.25;
[R.d0, R.d1, R.d2] = getRSet(R.model, R.gamma, true);
%%%
% * ��������� ���������� ��������� ������:
psi0.v = 2; % ������� �������� ������
psi0.w = 1; % ������� ������ ������
%% ����������� ���������� �������
% � ������ ������� ������������ ��������� �������� �����
x.net(:,1) = R.d0(0).*xi.net;
psi = zeros(xi.n, t.n);
phi = zeros(xi.n, t.n);
psi(:,1) = initialPacket(x.net(:,1), psi0.v, psi0.w);
phi(:,1) = transformScale(psi(:,1), x.net(:,1), R.d0(0), R.d1(0));
%%%
% ������ �������� ������ ���������� ��������� ������
figure;
    plot(xi.net, abs(phi(:,1)).^2);
    xlabel('x');
    ylabel('|\psi_0(x)|^2')
%% ������ �������� ��������� ������
% � ������ ������� ������������ ���������� �������� �� �������. ��� ������
% �������� ������������ ������� ��������������� ��������� ���������� � 
% ������� ����������� � ������������ �������� ���������� �������������� 
% �������
error = zeros(t.n, 1);
error(1) = sum(abs(...
        psi(:,1)-analyticalSolution(x.net(:,1),t.net(1), psi0.v, psi0.w)...
        ).^2);
for j=2:t.n
    S = getScaledS(xi.n, xi.h, R.d0, R.d2, t.net(j-1), t.net(j), xi.net);
    phi(:,j) = oneTimeStep(phi(:,j-1), S);
    [psi(:,j), x.net(:,j)] = ...
        transformScale(phi(:,j), xi.net, R.d0(t.net(j)), R.d1(t.net(j)), true);
    error(j) = sum(abs(...
        psi(:,j)-analyticalSolution(x.net(:,j),t.net(j), psi0.v, psi0.w)...
        ).^2);
end

%% ����� ���������� ������� � xi �����������
hold on;
    plot(xi.net, abs(phi(:,round(t.n/2))).^2);
    plot(xi.net, abs(phi(:,t.n)).^2);
    ylabel('|\psi(t,x)|^2')
    legend('t=0',...
        ['t=',num2str(t.net(round(t.n/2)))],...
        ['t=',num2str(t.max)])

%% ��������� ������� � �������������
figure();
hold on;
    plot(x.net(:,round(t.n/2)), abs(psi(:,round(t.n/2))).^2);
    plot(x.net(:,round(t.n/2)), abs(analyticalSolution(x.net(:,round(t.n/2)),t.net(round(t.n/2)),...
        psi0.v, psi0.w)).^2);
    plot(x.net(:,t.n), abs(psi(:,t.n)).^2);
    plot(x.net(:,t.n), abs(analyticalSolution(x.net(:,t.n),t.max, psi0.v, psi0.w)).^2);
    ylabel('|\psi(t,x)|^2')
    legend(...
        ['t=',num2str(t.net(round(t.n/2)))],...
        ['t=',num2str(t.net(round(t.n/2))),' analitical'],...
        ['t=',num2str(t.max)],...
        ['t=',num2str(t.max),' analitical'])
    
 %% ����������� ������ ���������� �� �������
 